# MIOPYC: Marintiek IO PYthon paCkage
=====================================

At MARINTEK, several in-house result file formats have been created over the
years. This project is intended to host python modules for reading these
files in a pythonic fashion.
