# Testing

Functionality that should be tested:

- DuplicateKeyError for duplicate matrix names



# Deprecated package `collections`

The python package `collections` is used. The `collections` package
is deprecated and will be removed in python version 3.10.
This will require a fix.
